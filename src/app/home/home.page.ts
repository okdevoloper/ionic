import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Board } from './pages/models/board.models';
import { Column } from './pages/models/column.models';
import autoScroll from 'dom-autoscroller'
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  activeIndex: number = 0;
  @ViewChild("slides", { static: false }) slides: IonSlides;
  slideOpts = {
    initialSlide: 0,
    speed: 100,
  };


  scroll:any;
  
  board: Board = new Board('Test Board', [
    new Column('Ideas', [
      'Some random idea',
      'This is another random idea',
      'build an awesome app',
    ]),
    new Column('Research', [
      'lorem ipsum',
      'foo',
      'this was in the bla bla bla',
    ]),
    new Column('In Progress', ['drag', 'drop', 'work']),
    new Column('to do', [
      'Get to work',
      'Pick up groceries',
      'Go home',
      'Fall asleep',
    ]),
    new Column('done', [
      'Get up',
      'Brush teeth',
      'Take a shower',
      'Check e-mail',
      'Walk dog',
    ]),
  ]);
  

  constructor(private router: Router) {}

  ngOnInit() {
    var scroll = autoScroll(
      [
        document.querySelector('#tasks'),
      ],
      {
        margin: 200,
        maxSpeed: 25,
        scrollWhenOutside: true,
        autoScroll: function () {
          console.log(scroll);
          return true;
        },
      }
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      console.log(event);
    }
  }

  navigate(){
    this.router.navigateByUrl("try-dragula")
  }
}
